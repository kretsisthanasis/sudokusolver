<?php

Class SudokuSubGrid
{
	private $subGrid;
	private $rowStart;
	private $columnStart;

	function __construct($rowNumber, $columnNumber, $inputArray)
	{
		$this->subGrid = subGridPicker($rowNumber, $columnNumber, $inputArray);
	}
	
	function getSubGrid()
	{
		return $this->subGrid;
	}
	
	//extracts and returns the subgrid from the input array
	function SubGridPicker($rowNumber, $columnNumber, $inputArray)
	{
		subGridStartPicker($rowNumber, $columnNumber);
		$result = array();
		
		for ($i=0;$i<3;$i++){
			for ($j=0;$j<3;$j++){
				$result[$i][$j] = $inputArray[$rowStart + $i][$columnStart + $j];
			}
		}
		
		return result;
	}
	
	//picks the starting row and column number of the input array subgrid
	function subGridStartPicker($rowNumber, $columnNumber)
	{
		if($rowNumber<3){
			if($columnNumber<3){
				$this->rowStart = 0;
				$this->columnStart = 0;
			}elseif($columnNumber<6){
				$this->rowStart = 0;
				$this->columnStart = 3;
			}else{
				$this->rowStart = 0;
				$this->columnStart = 6;
			}
		}elseif($rowNumber<6){
			if($columnNumber<3){
				$this->rowStart = 3;
				$this->columnStart = 0;
			}elseif($columnNumber<6){
				$this->rowStart = 3;
				$this->columnStart = 3;
			}else{
				$this->rowStart = 3;
				$this->columnStart = 6;
			}
		}else{
			if($columnNumber<3){
				$this->rowStart = 6;
				$this->columnStart = 0;
			}elseif($columnNumber<6){
				$this->rowStart = 6;
				$this->columnStart = 3;
			}else{
				$this->rowStart = 6;
				$this->columnStart = 6;
			}
		}
	}
}
