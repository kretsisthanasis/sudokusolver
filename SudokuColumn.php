<?php

Class SudokuColumn
{
	private $column;

	function __construct($columnNumber, $inputArray)
	{
		for ($i=0;$i<9;$i++){
			$this->column[$i] = $inputArray[$i][$columnNumber];
		}
	}
	
	function getColumn()
	{
		return $this->column;
	}
}
