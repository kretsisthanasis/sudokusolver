<?php

Class SudokuRow
{
	private $row;

	function __construct($rowNumber, $inputArray)
	{
		for ($i=0;$i<9;$i++){
			$this->row[$i] = $inputArray[$rowNumber][$i];
		}
	}
	
	function getRow()
	{
		return $this->row;
	}
}
