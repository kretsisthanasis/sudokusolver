<?php

class SudokuGrid
{
    /**
     * @param $input
     * @return array
     */
    public function fromString($input)
    {
		require_once('SudokuSquare.php');
		require_once('SudokuRow.php');
		require_once('SudokuColumn.php');
		require_once('SudokuSubGrid.php');
		
		//First step, we initiallise our variables.
        $flatArray = array();
        $sudokuArray = array();
		$sudokuGrid = array();
        $indexCount = 0;
        
		//Second step, we store the input string into a one-dimensional array
		for ($i=0;$i<=strlen($input)-1;$i++){
            $flatArray[$i] = $input[$i];
        }
		
		//Third Step, we convert the flat array into a two dimensional array
        for ($i=0;$i<9;$i++){
            for ($j=0;$j<9;$j++){
                $sudokuArray[$i][$j] = $flatArray[$indexCount];
                $indexCount++;
            }
        }

		//Fourth step, we create an object two dimensional array
		for ($i=0;$i<9;$i++){
			for ($j=0;$j<9;$j++){
				$sudokuRow = new SudokuRow($i, $sudokuArray);
				$sudokuColumn = new SudokuColumn($j, $sudokuArray);
				$sudokuSubGrid = new SudokuSubGrid($i, $j, $sudokuArray);
				$sudokuSquare = new SudokuSquare(
						$sudokuArray[$i][$j],
						$sudokuRow->getRow(),
						$sudokuColumn->getColumn(),
						$sudokuSubGrid->getSubGrid()
					);
				$sudokuGrid[$i][$j] = $sudokuSquare;
			}
		}
		
		return $sudokuGrid;
    }
}
