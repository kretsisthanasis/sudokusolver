<?php

Class SudokuSquare
{
	private $value;
	private $row;
	private $column;
	private $subGrid;
	
	function __construct($value, $row, $column, $subGrid)
	{
		$this->value = $value;
		$this->row = row;
		$this->column = $column;
		$this->subGrid = $subGrid;
	}
}
